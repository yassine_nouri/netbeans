/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO USER
 */
@Entity
@Table(name = "livres")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Livres.findAll", query = "SELECT l FROM Livres l"),
    @NamedQuery(name = "Livres.findByCodeLivre", query = "SELECT l FROM Livres l WHERE l.codeLivre = :codeLivre"),
    @NamedQuery(name = "Livres.findByTitre", query = "SELECT l FROM Livres l WHERE l.titre = :titre"),
    @NamedQuery(name = "Livres.findByAuteur", query = "SELECT l FROM Livres l WHERE l.auteur = :auteur"),
    @NamedQuery(name = "Livres.findBy\u00c9diteur", query = "SELECT l FROM Livres l WHERE l.\u00e9diteur = :\u00e9diteur"),
    @NamedQuery(name = "Livres.findByCode\u00c9diteur", query = "SELECT l FROM Livres l WHERE l.code\u00c9diteur = :code\u00c9diteur")})
public class Livres implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "code_livre")
    private String codeLivre;
    @Size(max = 45)
    @Column(name = "titre")
    private String titre;
    @Size(max = 20)
    @Column(name = "auteur")
    private String auteur;
    @Size(max = 20)
    @Column(name = "\u00e9diteur")
    private String éditeur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "code_\u00e9diteur")
    private String codeÉditeur;

    public Livres() {
    }

    public Livres(String codeLivre) {
        this.codeLivre = codeLivre;
    }

    public Livres(String codeLivre, String codeÉditeur) {
        this.codeLivre = codeLivre;
        this.codeÉditeur = codeÉditeur;
    }

    public String getCodeLivre() {
        return codeLivre;
    }

    public void setCodeLivre(String codeLivre) {
        this.codeLivre = codeLivre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getÉditeur() {
        return éditeur;
    }

    public void setÉditeur(String éditeur) {
        this.éditeur = éditeur;
    }

    public String getCodeÉditeur() {
        return codeÉditeur;
    }

    public void setCodeÉditeur(String codeÉditeur) {
        this.codeÉditeur = codeÉditeur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeLivre != null ? codeLivre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Livres)) {
            return false;
        }
        Livres other = (Livres) object;
        if ((this.codeLivre == null && other.codeLivre != null) || (this.codeLivre != null && !this.codeLivre.equals(other.codeLivre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.Livres[ codeLivre=" + codeLivre + " ]";
    }
    
}
