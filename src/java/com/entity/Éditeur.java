/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO USER
 */
@Entity
@Table(name = "\u00e9diteur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "\u00c9diteur.findAll", query = "SELECT \u00e9 FROM \u00c9diteur \u00e9"),
    @NamedQuery(name = "\u00c9diteur.findBy\u00c9diteur", query = "SELECT \u00e9 FROM \u00c9diteur \u00e9 WHERE \u00e9.\u00e9diteur = :\u00e9diteur"),
    @NamedQuery(name = "\u00c9diteur.findByCodePostal", query = "SELECT \u00e9 FROM \u00c9diteur \u00e9 WHERE \u00e9.codePostal = :codePostal"),
    @NamedQuery(name = "\u00c9diteur.findByCode\u00c9diteur", query = "SELECT \u00e9 FROM \u00c9diteur \u00e9 WHERE \u00e9.code\u00c9diteur = :code\u00c9diteur"),
    @NamedQuery(name = "\u00c9diteur.findByAddress", query = "SELECT \u00e9 FROM \u00c9diteur \u00e9 WHERE \u00e9.address = :address")})
public class Éditeur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "\u00e9diteur")
    private String éditeur;
    @Size(max = 15)
    @Column(name = "code_postal")
    private String codePostal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "code_\u00e9diteur")
    private String codeÉditeur;
    @Size(max = 15)
    @Column(name = "address")
    private String address;

    public Éditeur() {
    }

    public Éditeur(String éditeur) {
        this.éditeur = éditeur;
    }

    public Éditeur(String éditeur, String codeÉditeur) {
        this.éditeur = éditeur;
        this.codeÉditeur = codeÉditeur;
    }

    public String getÉditeur() {
        return éditeur;
    }

    public void setÉditeur(String éditeur) {
        this.éditeur = éditeur;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getCodeÉditeur() {
        return codeÉditeur;
    }

    public void setCodeÉditeur(String codeÉditeur) {
        this.codeÉditeur = codeÉditeur;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (éditeur != null ? éditeur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Éditeur)) {
            return false;
        }
        Éditeur other = (Éditeur) object;
        if ((this.éditeur == null && other.éditeur != null) || (this.éditeur != null && !this.éditeur.equals(other.éditeur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.\u00c9diteur[ \u00e9diteur=" + éditeur + " ]";
    }
    
}
