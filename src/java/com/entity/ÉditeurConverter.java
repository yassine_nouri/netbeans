/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author LENOVO USER
 */
public class ÉditeurConverter implements Converter {

    public Object getAsObject(FacesContext facesContext, UIComponent component, String string) {
        if (string == null || string.length() == 0) {
            return null;
        }
        String id = string;
        ÉditeurController controller = (ÉditeurController) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "\u00e9diteur");
        return controller.getJpaController().find(id);
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Éditeur) {
            Éditeur o = (Éditeur) object;
            return o.getÉditeur() == null ? "" : o.getÉditeur().toString();
        } else {
            throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: com.entity.\u00c9diteur");
        }
    }
    
}
