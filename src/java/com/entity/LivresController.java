/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.faces.FacesException;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;
import com.entity.util.JsfUtil;
import com.entity.util.PagingInfo;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author LENOVO USER
 */
public class LivresController {

    public LivresController() {
        pagingInfo = new PagingInfo();
        converter = new LivresConverter();
    }
    private Livres livres = null;
    private List<Livres> livresItems = null;
    private LivresFacade jpaController = null;
    private LivresConverter converter = null;
    private PagingInfo pagingInfo = null;
    @Resource
    private UserTransaction utx = null;
    @PersistenceUnit(unitName = "mylibraryPU")
    private EntityManagerFactory emf = null;

    public PagingInfo getPagingInfo() {
        if (pagingInfo.getItemCount() == -1) {
            pagingInfo.setItemCount(getJpaController().count());
        }
        return pagingInfo;
    }

    public LivresFacade getJpaController() {
        if (jpaController == null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            jpaController = (LivresFacade) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "livresJpa");
        }
        return jpaController;
    }

    public SelectItem[] getLivresItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), false);
    }

    public SelectItem[] getLivresItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), true);
    }

    public Livres getLivres() {
        if (livres == null) {
            livres = (Livres) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentLivres", converter, null);
        }
        if (livres == null) {
            livres = new Livres();
        }
        return livres;
    }

    public String listSetup() {
        reset(true);
        return "livres_list";
    }

    public String createSetup() {
        reset(false);
        livres = new Livres();
        return "livres_create";
    }

    public String create() {
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().create(livres);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Livres was successfully created.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return listSetup();
    }

    public String detailSetup() {
        return scalarSetup("livres_detail");
    }

    public String editSetup() {
        return scalarSetup("livres_edit");
    }

    private String scalarSetup(String destination) {
        reset(false);
        livres = (Livres) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentLivres", converter, null);
        if (livres == null) {
            String requestLivresString = JsfUtil.getRequestParameter("jsfcrud.currentLivres");
            JsfUtil.addErrorMessage("The livres with id " + requestLivresString + " no longer exists.");
            return relatedOrListOutcome();
        }
        return destination;
    }

    public String edit() {
        String livresString = converter.getAsString(FacesContext.getCurrentInstance(), null, livres);
        String currentLivresString = JsfUtil.getRequestParameter("jsfcrud.currentLivres");
        if (livresString == null || livresString.length() == 0 || !livresString.equals(currentLivresString)) {
            String outcome = editSetup();
            if ("livres_edit".equals(outcome)) {
                JsfUtil.addErrorMessage("Could not edit livres. Try again.");
            }
            return outcome;
        }
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().edit(livres);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Livres was successfully updated.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return detailSetup();
    }

    public String remove() {
        String idAsString = JsfUtil.getRequestParameter("jsfcrud.currentLivres");
        String id = idAsString;
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().remove(getJpaController().find(id));
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Livres was successfully deleted.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return relatedOrListOutcome();
    }

    private String relatedOrListOutcome() {
        String relatedControllerOutcome = relatedControllerOutcome();
        if ((ERROR)) {
            return relatedControllerOutcome;
        }
        return listSetup();
    }

    public List<Livres> getLivresItems() {
        if (livresItems == null) {
            getPagingInfo();
            livresItems = getJpaController().findRange(new int[]{pagingInfo.getFirstItem(), pagingInfo.getFirstItem() + pagingInfo.getBatchSize()});
        }
        return livresItems;
    }

    public String next() {
        reset(false);
        getPagingInfo().nextPage();
        return "livres_list";
    }

    public String prev() {
        reset(false);
        getPagingInfo().previousPage();
        return "livres_list";
    }

    private String relatedControllerOutcome() {
        String relatedControllerString = JsfUtil.getRequestParameter("jsfcrud.relatedController");
        String relatedControllerTypeString = JsfUtil.getRequestParameter("jsfcrud.relatedControllerType");
        if (relatedControllerString != null && relatedControllerTypeString != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            Object relatedController = context.getApplication().getELResolver().getValue(context.getELContext(), null, relatedControllerString);
            try {
                Class<?> relatedControllerType = Class.forName(relatedControllerTypeString);
                Method detailSetupMethod = relatedControllerType.getMethod("detailSetup");
                return (String) detailSetupMethod.invoke(relatedController);
            } catch (ClassNotFoundException e) {
                throw new FacesException(e);
            } catch (NoSuchMethodException e) {
                throw new FacesException(e);
            } catch (IllegalAccessException e) {
                throw new FacesException(e);
            } catch (InvocationTargetException e) {
                throw new FacesException(e);
            }
        }
        return null;
    }

    private void reset(boolean resetFirstItem) {
        livres = null;
        livresItems = null;
        pagingInfo.setItemCount(-1);
        if (resetFirstItem) {
            pagingInfo.setFirstItem(0);
        }
    }

    public void validateCreate(FacesContext facesContext, UIComponent component, Object value) {
        Livres newLivres = new Livres();
        String newLivresString = converter.getAsString(FacesContext.getCurrentInstance(), null, newLivres);
        String livresString = converter.getAsString(FacesContext.getCurrentInstance(), null, livres);
        if (!newLivresString.equals(livresString)) {
            createSetup();
        }
    }

    public Converter getConverter() {
        return converter;
    }
    
}
