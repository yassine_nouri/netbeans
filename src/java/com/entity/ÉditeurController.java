/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.faces.FacesException;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;
import com.entity.util.JsfUtil;
import com.entity.util.PagingInfo;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author LENOVO USER
 */
public class ÉditeurController {

    public ÉditeurController() {
        pagingInfo = new PagingInfo();
        converter = new ÉditeurConverter();
    }
    private Éditeur éditeur = null;
    private List<Éditeur> éditeurItems = null;
    private ÉditeurFacade jpaController = null;
    private ÉditeurConverter converter = null;
    private PagingInfo pagingInfo = null;
    @Resource
    private UserTransaction utx = null;
    @PersistenceUnit(unitName = "mylibraryPU")
    private EntityManagerFactory emf = null;

    public PagingInfo getPagingInfo() {
        if (pagingInfo.getItemCount() == -1) {
            pagingInfo.setItemCount(getJpaController().count());
        }
        return pagingInfo;
    }

    public ÉditeurFacade getJpaController() {
        if (jpaController == null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            jpaController = (ÉditeurFacade) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "\u00e9diteurJpa");
        }
        return jpaController;
    }

    public SelectItem[] getÉditeurItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), false);
    }

    public SelectItem[] getÉditeurItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), true);
    }

    public Éditeur getÉditeur() {
        if (éditeur == null) {
            éditeur = (Éditeur) JsfUtil.getObjectFromRequestParameter("jsfcrud.current\u00c9diteur", converter, null);
        }
        if (éditeur == null) {
            éditeur = new Éditeur();
        }
        return éditeur;
    }

    public String listSetup() {
        reset(true);
        return "\u00e9diteur_list";
    }

    public String createSetup() {
        reset(false);
        éditeur = new Éditeur();
        return "\u00e9diteur_create";
    }

    public String create() {
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().create(éditeur);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("\u00c9diteur was successfully created.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return listSetup();
    }

    public String detailSetup() {
        return scalarSetup("\u00e9diteur_detail");
    }

    public String editSetup() {
        return scalarSetup("\u00e9diteur_edit");
    }

    private String scalarSetup(String destination) {
        reset(false);
        éditeur = (Éditeur) JsfUtil.getObjectFromRequestParameter("jsfcrud.current\u00c9diteur", converter, null);
        if (éditeur == null) {
            String requestÉditeurString = JsfUtil.getRequestParameter("jsfcrud.current\u00c9diteur");
            JsfUtil.addErrorMessage("The \u00e9diteur with id " + requestÉditeurString + " no longer exists.");
            return relatedOrListOutcome();
        }
        return destination;
    }

    public String edit() {
        String éditeurString = converter.getAsString(FacesContext.getCurrentInstance(), null, éditeur);
        String currentÉditeurString = JsfUtil.getRequestParameter("jsfcrud.current\u00c9diteur");
        if (éditeurString == null || éditeurString.length() == 0 || !éditeurString.equals(currentÉditeurString)) {
            String outcome = editSetup();
            if ("\u00e9diteur_edit".equals(outcome)) {
                JsfUtil.addErrorMessage("Could not edit \u00e9diteur. Try again.");
            }
            return outcome;
        }
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().edit(éditeur);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("\u00c9diteur was successfully updated.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return detailSetup();
    }

    public String remove() {
        String idAsString = JsfUtil.getRequestParameter("jsfcrud.current\u00c9diteur");
        String id = idAsString;
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().remove(getJpaController().find(id));
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("\u00c9diteur was successfully deleted.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return relatedOrListOutcome();
    }

    private String relatedOrListOutcome() {
        String relatedControllerOutcome = relatedControllerOutcome();
        if ((ERROR)) {
            return relatedControllerOutcome;
        }
        return listSetup();
    }

    public List<Éditeur> getÉditeurItems() {
        if (éditeurItems == null) {
            getPagingInfo();
            éditeurItems = getJpaController().findRange(new int[]{pagingInfo.getFirstItem(), pagingInfo.getFirstItem() + pagingInfo.getBatchSize()});
        }
        return éditeurItems;
    }

    public String next() {
        reset(false);
        getPagingInfo().nextPage();
        return "\u00e9diteur_list";
    }

    public String prev() {
        reset(false);
        getPagingInfo().previousPage();
        return "\u00e9diteur_list";
    }

    private String relatedControllerOutcome() {
        String relatedControllerString = JsfUtil.getRequestParameter("jsfcrud.relatedController");
        String relatedControllerTypeString = JsfUtil.getRequestParameter("jsfcrud.relatedControllerType");
        if (relatedControllerString != null && relatedControllerTypeString != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            Object relatedController = context.getApplication().getELResolver().getValue(context.getELContext(), null, relatedControllerString);
            try {
                Class<?> relatedControllerType = Class.forName(relatedControllerTypeString);
                Method detailSetupMethod = relatedControllerType.getMethod("detailSetup");
                return (String) detailSetupMethod.invoke(relatedController);
            } catch (ClassNotFoundException e) {
                throw new FacesException(e);
            } catch (NoSuchMethodException e) {
                throw new FacesException(e);
            } catch (IllegalAccessException e) {
                throw new FacesException(e);
            } catch (InvocationTargetException e) {
                throw new FacesException(e);
            }
        }
        return null;
    }

    private void reset(boolean resetFirstItem) {
        éditeur = null;
        éditeurItems = null;
        pagingInfo.setItemCount(-1);
        if (resetFirstItem) {
            pagingInfo.setFirstItem(0);
        }
    }

    public void validateCreate(FacesContext facesContext, UIComponent component, Object value) {
        Éditeur newÉditeur = new Éditeur();
        String newÉditeurString = converter.getAsString(FacesContext.getCurrentInstance(), null, newÉditeur);
        String éditeurString = converter.getAsString(FacesContext.getCurrentInstance(), null, éditeur);
        if (!newÉditeurString.equals(éditeurString)) {
            createSetup();
        }
    }

    public Converter getConverter() {
        return converter;
    }
    
}
