<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>New User</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>New User</h1>
            <h:form>
                <h:inputHidden id="validateCreateField" validator="#{user.validateCreate}" value="value"/>
                <h:panelGrid columns="2">
                    <h:outputText value="IdUser:"/>
                    <h:inputText id="idUser" value="#{user.user.idUser}" title="IdUser" required="true" requiredMessage="The idUser field is required." />
                    <h:outputText value="Nom:"/>
                    <h:inputText id="nom" value="#{user.user.nom}" title="Nom" required="true" requiredMessage="The nom field is required." />
                    <h:outputText value="Age:"/>
                    <h:inputText id="age" value="#{user.user.age}" title="Age" required="true" requiredMessage="The age field is required." />
                    <h:outputText value="Sex:"/>
                    <h:inputText id="sex" value="#{user.user.sex}" title="Sex" required="true" requiredMessage="The sex field is required." />
                    <h:outputText value="Address:"/>
                    <h:inputText id="address" value="#{user.user.address}" title="Address" required="true" requiredMessage="The address field is required." />

                </h:panelGrid>
                <br />
                <h:commandLink action="#{user.create}" value="Create"/>
                <br />
                <br />
                <h:commandLink action="#{user.listSetup}" value="Show All User Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
