<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>User Detail</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>User Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdUser:"/>
                    <h:outputText value="#{user.user.idUser}" title="IdUser" />
                    <h:outputText value="Nom:"/>
                    <h:outputText value="#{user.user.nom}" title="Nom" />
                    <h:outputText value="Age:"/>
                    <h:outputText value="#{user.user.age}" title="Age" />
                    <h:outputText value="Sex:"/>
                    <h:outputText value="#{user.user.sex}" title="Sex" />
                    <h:outputText value="Address:"/>
                    <h:outputText value="#{user.user.address}" title="Address" />


                </h:panelGrid>
                <br />
                <h:commandLink action="#{user.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentUser" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][user.user][user.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{user.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentUser" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][user.user][user.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{user.createSetup}" value="New User" />
                <br />
                <h:commandLink action="#{user.listSetup}" value="Show All User Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
