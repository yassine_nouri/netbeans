<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Editing User</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Editing User</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdUser:"/>
                    <h:outputText value="#{user.user.idUser}" title="IdUser" />
                    <h:outputText value="Nom:"/>
                    <h:inputText id="nom" value="#{user.user.nom}" title="Nom" required="true" requiredMessage="The nom field is required." />
                    <h:outputText value="Age:"/>
                    <h:inputText id="age" value="#{user.user.age}" title="Age" required="true" requiredMessage="The age field is required." />
                    <h:outputText value="Sex:"/>
                    <h:inputText id="sex" value="#{user.user.sex}" title="Sex" required="true" requiredMessage="The sex field is required." />
                    <h:outputText value="Address:"/>
                    <h:inputText id="address" value="#{user.user.address}" title="Address" required="true" requiredMessage="The address field is required." />

                </h:panelGrid>
                <br />
                <h:commandLink action="#{user.edit}" value="Save">
                    <f:param name="jsfcrud.currentUser" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][user.user][user.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{user.detailSetup}" value="Show" immediate="true">
                    <f:param name="jsfcrud.currentUser" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][user.user][user.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <h:commandLink action="#{user.listSetup}" value="Show All User Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
