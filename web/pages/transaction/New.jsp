<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>New Transaction</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>New Transaction</h1>
            <h:form>
                <h:inputHidden id="validateCreateField" validator="#{transaction.validateCreate}" value="value"/>
                <h:panelGrid columns="2">
                    <h:outputText value="IdTrans:"/>
                    <h:inputText id="idTrans" value="#{transaction.transaction.idTrans}" title="IdTrans" required="true" requiredMessage="The idTrans field is required." />
                    <h:outputText value="Date:"/>
                    <h:inputText id="date" value="#{transaction.transaction.date}" title="Date" required="true" requiredMessage="The date field is required." />
                    <h:outputText value="IdUser:"/>
                    <h:inputText id="idUser" value="#{transaction.transaction.idUser}" title="IdUser" required="true" requiredMessage="The idUser field is required." />
                    <h:outputText value="CodeLivre:"/>
                    <h:inputText id="codeLivre" value="#{transaction.transaction.codeLivre}" title="CodeLivre" required="true" requiredMessage="The codeLivre field is required." />

                </h:panelGrid>
                <br />
                <h:commandLink action="#{transaction.create}" value="Create"/>
                <br />
                <br />
                <h:commandLink action="#{transaction.listSetup}" value="Show All Transaction Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
