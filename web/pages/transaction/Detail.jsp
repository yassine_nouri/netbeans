<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Transaction Detail</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Transaction Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdTrans:"/>
                    <h:outputText value="#{transaction.transaction.idTrans}" title="IdTrans" />
                    <h:outputText value="Date:"/>
                    <h:outputText value="#{transaction.transaction.date}" title="Date" />
                    <h:outputText value="IdUser:"/>
                    <h:outputText value="#{transaction.transaction.idUser}" title="IdUser" />
                    <h:outputText value="CodeLivre:"/>
                    <h:outputText value="#{transaction.transaction.codeLivre}" title="CodeLivre" />


                </h:panelGrid>
                <br />
                <h:commandLink action="#{transaction.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentTransaction" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][transaction.transaction][transaction.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{transaction.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentTransaction" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][transaction.transaction][transaction.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{transaction.createSetup}" value="New Transaction" />
                <br />
                <h:commandLink action="#{transaction.listSetup}" value="Show All Transaction Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
