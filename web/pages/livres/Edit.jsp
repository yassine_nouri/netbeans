<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Editing Livres</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Editing Livres</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="CodeLivre:"/>
                    <h:outputText value="#{livres.livres.codeLivre}" title="CodeLivre" />
                    <h:outputText value="Titre:"/>
                    <h:inputText id="titre" value="#{livres.livres.titre}" title="Titre" />
                    <h:outputText value="Auteur:"/>
                    <h:inputText id="auteur" value="#{livres.livres.auteur}" title="Auteur" />
                    <h:outputText value="Éditeur:"/>
                    <h:inputText id="éditeur" value="#{livres.livres.éditeur}" title="Éditeur" />
                    <h:outputText value="CodeÉditeur:"/>
                    <h:inputText id="codeÉditeur" value="#{livres.livres.codeÉditeur}" title="CodeÉditeur" required="true" requiredMessage="The codeÉditeur field is required." />

                </h:panelGrid>
                <br />
                <h:commandLink action="#{livres.edit}" value="Save">
                    <f:param name="jsfcrud.currentLivres" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][livres.livres][livres.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{livres.detailSetup}" value="Show" immediate="true">
                    <f:param name="jsfcrud.currentLivres" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][livres.livres][livres.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <h:commandLink action="#{livres.listSetup}" value="Show All Livres Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
