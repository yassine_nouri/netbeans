<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>New Livres</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>New Livres</h1>
            <h:form>
                <h:inputHidden id="validateCreateField" validator="#{livres.validateCreate}" value="value"/>
                <h:panelGrid columns="2">
                    <h:outputText value="CodeLivre:"/>
                    <h:inputText id="codeLivre" value="#{livres.livres.codeLivre}" title="CodeLivre" required="true" requiredMessage="The codeLivre field is required." />
                    <h:outputText value="Titre:"/>
                    <h:inputText id="titre" value="#{livres.livres.titre}" title="Titre" />
                    <h:outputText value="Auteur:"/>
                    <h:inputText id="auteur" value="#{livres.livres.auteur}" title="Auteur" />
                    <h:outputText value="Éditeur:"/>
                    <h:inputText id="éditeur" value="#{livres.livres.éditeur}" title="Éditeur" />
                    <h:outputText value="CodeÉditeur:"/>
                    <h:inputText id="codeÉditeur" value="#{livres.livres.codeÉditeur}" title="CodeÉditeur" required="true" requiredMessage="The codeÉditeur field is required." />

                </h:panelGrid>
                <br />
                <h:commandLink action="#{livres.create}" value="Create"/>
                <br />
                <br />
                <h:commandLink action="#{livres.listSetup}" value="Show All Livres Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
