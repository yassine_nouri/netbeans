<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Livres Detail</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Livres Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="CodeLivre:"/>
                    <h:outputText value="#{livres.livres.codeLivre}" title="CodeLivre" />
                    <h:outputText value="Titre:"/>
                    <h:outputText value="#{livres.livres.titre}" title="Titre" />
                    <h:outputText value="Auteur:"/>
                    <h:outputText value="#{livres.livres.auteur}" title="Auteur" />
                    <h:outputText value="Éditeur:"/>
                    <h:outputText value="#{livres.livres.éditeur}" title="Éditeur" />
                    <h:outputText value="CodeÉditeur:"/>
                    <h:outputText value="#{livres.livres.codeÉditeur}" title="CodeÉditeur" />


                </h:panelGrid>
                <br />
                <h:commandLink action="#{livres.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentLivres" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][livres.livres][livres.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{livres.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentLivres" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][livres.livres][livres.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{livres.createSetup}" value="New Livres" />
                <br />
                <h:commandLink action="#{livres.listSetup}" value="Show All Livres Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
