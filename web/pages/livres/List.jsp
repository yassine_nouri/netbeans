<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Listing Livres Items</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Listing Livres Items</h1>
            <h:form styleClass="jsfcrud_list_form">
                <h:outputText escape="false" value="(No Livres Items Found)<br />" rendered="#{livres.pagingInfo.itemCount == 0}" />
                <h:panelGroup rendered="#{livres.pagingInfo.itemCount > 0}">
                    <h:outputText value="Item #{livres.pagingInfo.firstItem + 1}..#{livres.pagingInfo.lastItem} of #{livres.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{livres.prev}" value="Previous #{livres.pagingInfo.batchSize}" rendered="#{livres.pagingInfo.firstItem >= livres.pagingInfo.batchSize}"/>&nbsp;
                    <h:commandLink action="#{livres.next}" value="Next #{livres.pagingInfo.batchSize}" rendered="#{livres.pagingInfo.lastItem + livres.pagingInfo.batchSize <= livres.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{livres.next}" value="Remaining #{livres.pagingInfo.itemCount - livres.pagingInfo.lastItem}"
                                   rendered="#{livres.pagingInfo.lastItem < livres.pagingInfo.itemCount && livres.pagingInfo.lastItem + livres.pagingInfo.batchSize > livres.pagingInfo.itemCount}"/>
                    <h:dataTable value="#{livres.livresItems}" var="item" border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px">
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="CodeLivre"/>
                            </f:facet>
                            <h:outputText value="#{item.codeLivre}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Titre"/>
                            </f:facet>
                            <h:outputText value="#{item.titre}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Auteur"/>
                            </f:facet>
                            <h:outputText value="#{item.auteur}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Éditeur"/>
                            </f:facet>
                            <h:outputText value="#{item.éditeur}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="CodeÉditeur"/>
                            </f:facet>
                            <h:outputText value="#{item.codeÉditeur}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText escape="false" value="&nbsp;"/>
                            </f:facet>
                            <h:commandLink value="Show" action="#{livres.detailSetup}">
                                <f:param name="jsfcrud.currentLivres" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][livres.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{livres.editSetup}">
                                <f:param name="jsfcrud.currentLivres" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][livres.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{livres.remove}">
                                <f:param name="jsfcrud.currentLivres" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][livres.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                        </h:column>

                    </h:dataTable>
                </h:panelGroup>
                <br />
                <h:commandLink action="#{livres.createSetup}" value="New Livres"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />


            </h:form>
        </body>
    </html>
</f:view>
