<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Listing Éditeur Items</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Listing Éditeur Items</h1>
            <h:form styleClass="jsfcrud_list_form">
                <h:outputText escape="false" value="(No Éditeur Items Found)<br />" rendered="#{éditeur.pagingInfo.itemCount == 0}" />
                <h:panelGroup rendered="#{éditeur.pagingInfo.itemCount > 0}">
                    <h:outputText value="Item #{éditeur.pagingInfo.firstItem + 1}..#{éditeur.pagingInfo.lastItem} of #{éditeur.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{éditeur.prev}" value="Previous #{éditeur.pagingInfo.batchSize}" rendered="#{éditeur.pagingInfo.firstItem >= éditeur.pagingInfo.batchSize}"/>&nbsp;
                    <h:commandLink action="#{éditeur.next}" value="Next #{éditeur.pagingInfo.batchSize}" rendered="#{éditeur.pagingInfo.lastItem + éditeur.pagingInfo.batchSize <= éditeur.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{éditeur.next}" value="Remaining #{éditeur.pagingInfo.itemCount - éditeur.pagingInfo.lastItem}"
                                   rendered="#{éditeur.pagingInfo.lastItem < éditeur.pagingInfo.itemCount && éditeur.pagingInfo.lastItem + éditeur.pagingInfo.batchSize > éditeur.pagingInfo.itemCount}"/>
                    <h:dataTable value="#{éditeur.éditeurItems}" var="item" border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px">
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Éditeur"/>
                            </f:facet>
                            <h:outputText value="#{item.éditeur}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="CodePostal"/>
                            </f:facet>
                            <h:outputText value="#{item.codePostal}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="CodeÉditeur"/>
                            </f:facet>
                            <h:outputText value="#{item.codeÉditeur}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Address"/>
                            </f:facet>
                            <h:outputText value="#{item.address}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText escape="false" value="&nbsp;"/>
                            </f:facet>
                            <h:commandLink value="Show" action="#{éditeur.detailSetup}">
                                <f:param name="jsfcrud.currentÉditeur" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][éditeur.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{éditeur.editSetup}">
                                <f:param name="jsfcrud.currentÉditeur" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][éditeur.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{éditeur.remove}">
                                <f:param name="jsfcrud.currentÉditeur" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][éditeur.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                        </h:column>

                    </h:dataTable>
                </h:panelGroup>
                <br />
                <h:commandLink action="#{éditeur.createSetup}" value="New Éditeur"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />


            </h:form>
        </body>
    </html>
</f:view>
