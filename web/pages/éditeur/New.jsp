<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>New Éditeur</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>New Éditeur</h1>
            <h:form>
                <h:inputHidden id="validateCreateField" validator="#{éditeur.validateCreate}" value="value"/>
                <h:panelGrid columns="2">
                    <h:outputText value="Éditeur:"/>
                    <h:inputText id="éditeur" value="#{éditeur.éditeur.éditeur}" title="Éditeur" required="true" requiredMessage="The éditeur field is required." />
                    <h:outputText value="CodePostal:"/>
                    <h:inputText id="codePostal" value="#{éditeur.éditeur.codePostal}" title="CodePostal" />
                    <h:outputText value="CodeÉditeur:"/>
                    <h:inputText id="codeÉditeur" value="#{éditeur.éditeur.codeÉditeur}" title="CodeÉditeur" required="true" requiredMessage="The codeÉditeur field is required." />
                    <h:outputText value="Address:"/>
                    <h:inputText id="address" value="#{éditeur.éditeur.address}" title="Address" />

                </h:panelGrid>
                <br />
                <h:commandLink action="#{éditeur.create}" value="Create"/>
                <br />
                <br />
                <h:commandLink action="#{éditeur.listSetup}" value="Show All Éditeur Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
