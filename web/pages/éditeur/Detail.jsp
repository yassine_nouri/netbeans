<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Éditeur Detail</title>
            <link rel="stylesheet" type="text/css" href="/mylibrary/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Éditeur Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="Éditeur:"/>
                    <h:outputText value="#{éditeur.éditeur.éditeur}" title="Éditeur" />
                    <h:outputText value="CodePostal:"/>
                    <h:outputText value="#{éditeur.éditeur.codePostal}" title="CodePostal" />
                    <h:outputText value="CodeÉditeur:"/>
                    <h:outputText value="#{éditeur.éditeur.codeÉditeur}" title="CodeÉditeur" />
                    <h:outputText value="Address:"/>
                    <h:outputText value="#{éditeur.éditeur.address}" title="Address" />


                </h:panelGrid>
                <br />
                <h:commandLink action="#{éditeur.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentÉditeur" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][éditeur.éditeur][éditeur.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{éditeur.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentÉditeur" value="#{jsfcrud_class['com.entity.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][éditeur.éditeur][éditeur.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{éditeur.createSetup}" value="New Éditeur" />
                <br />
                <h:commandLink action="#{éditeur.listSetup}" value="Show All Éditeur Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>
